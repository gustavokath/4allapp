package br.com.gustavokath.a4allapp;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import java.io.IOException;

import br.com.gustavokath.a4allapp.entities.Task;

import static org.junit.Assert.assertEquals;

/**
 * Created by gustavokath on 03/02/17.
 */

public class DeserializeTaskTest {

    @Test
    public void givenBidirectionRelation_whenDeserializingWithIdentity_thenCorrect()
            throws IOException {
        String json = "{\"id\":\"1\",\"cidade\":\"Porto Alegre\",\"bairro\":\"Moinhos de Vento\",\"urlFoto\":\"http://dev.4all.mobi:3003/foto_01.jpg\",\"urlLogo\":\"http://dev.4all.mobi:3003/logo_01.png\",\"titulo\":\"Lorem Ipsum\",\"telefone\":\"5122333311\",\"texto\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus consequat nulla, a laoreet ipsum blandit ac. Donec vitae convallis nisi. Mauris molestie id lorem quis dignissim. Cum sociis natoque penatibus et magnis dis parturient montes.\",\"endereco\":\"Avenida Carlos Gomes, 532\",\"latitude\":-30.0306551,\"longitude\":-51.1846846,\"comentarios\":[{\"urlFoto\":\"http://dev.4all.mobi:3003/usuario1.jpeg\",\"nome\":\"Lorem Ipsum\",\"nota\":2,\"titulo\":\"Consequat Nulla\",\"comentario\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus consequat nulla, a laoreet ipsum blandit ac. Donec vitae convallis nisi.\"},{\"urlFoto\":\"http://dev.4all.mobi:3003/usuario2.jpeg\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"}]}";


        Task task = new ObjectMapper().readValue(json, Task.class);

        assertEquals("1", task.getId());
        assertEquals("Porto Alegre", task.getCity());
        assertEquals("Moinhos de Vento", task.getNeighborhood());
        assertEquals("http://dev.4all.mobi:3003/foto_01.jpg", task.getUrlPicture());
        assertEquals("http://dev.4all.mobi:3003/logo_01.png", task.getUrlLogo());
        assertEquals("Lorem Ipsum", task.getTitle());
        assertEquals("5122333311", task.getPhoneNumber());
        assertEquals("Avenida Carlos Gomes, 532", task.getAddress());

        assertEquals("Consequat Nulla", task.getCommentsList().get(0).getTitle());
        assertEquals("finibus consequat", task.getCommentsList().get(1).getTitle());

        assertEquals(2, task.getCommentsList().size());


    }
}
