package br.com.gustavokath.a4allapp.async;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;

import br.com.gustavokath.a4allapp.api.HttpClient;
import br.com.gustavokath.a4allapp.constants.Constants;
import br.com.gustavokath.a4allapp.entities.Task;
import br.com.gustavokath.a4allapp.entities.TaskIdList;
import okhttp3.Response;

/**
 * Created by gustavokath on 02/02/17.
 */

public class AsyncGetTaskIdList extends AsyncHttpRequest<String, Void, TaskIdList> {

    public AsyncGetTaskIdList(Handler handler) {
        super(handler);
    }

    @Override
    protected TaskIdList doInBackground(String... params) {
        URL url = null;
        TaskIdList taskIdList = null;

        url = buildUrl(Constants.HTTP_PROTOCOL, Constants.API_4ALL_URI, Constants.API_4ALL_PORT, params[0]);
        if(url == null)
            return null;

        Response response = getRequest(url);
        if(response == null)
            return null;

        if(response.isSuccessful()){
            try {
                taskIdList = new ObjectMapper().readValue(response.body().string(), TaskIdList.class);
            }catch (IOException exception){
                Log.e("ERROR","Fail to deserialize task");
            }
        }
        return taskIdList;
    }

    @Override
    protected void onPostExecute(TaskIdList taskIdList) {
        int messageCode;
        if(taskIdList == null){
            messageCode = Constants.ASYNC_JOB_FAILED;
        }else{
            messageCode = Constants.SUCCESSFULL_RESPONSE;
        }

        Message message = new Message();
        message.what = messageCode;
        message.obj = taskIdList;
        getHandler().sendMessage(message);
    }
}
