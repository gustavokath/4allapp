package br.com.gustavokath.a4allapp.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mikhaellopez.circularimageview.CircularImageView;

import br.com.gustavokath.a4allapp.R;
import br.com.gustavokath.a4allapp.adapters.CommentAdapter;
import br.com.gustavokath.a4allapp.async.AsyncGetTask;
import br.com.gustavokath.a4allapp.constants.Constants;
import br.com.gustavokath.a4allapp.entities.Task;

public class MainScreenActivity extends AppCompatActivity implements OnMapReadyCallback {
    private String taskId;
    private Handler handler;
    private ProgressBar loadingBar;
    private Task task;
    private TextView titleTextView, mapAddressTextView;
    private GoogleMap map;
    private ImageView pictureImageView;
    private CircularImageView logo;
    private ListView commentsListView;
    private ScrollView scrollView;
    private WebView textTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        taskId = intent.getStringExtra("id");
        loadingBar = (ProgressBar) findViewById(R.id.main_screen_progress_bar);
        titleTextView = (TextView) findViewById(R.id.main_screen_title);
        textTextView = (WebView) findViewById(R.id.main_screen_text);
        mapAddressTextView = (TextView) findViewById(R.id.main_screen_map_address);
        pictureImageView = (ImageView) findViewById(R.id.main_screen_picture_image_view);
        logo = (CircularImageView) findViewById(R.id.main_screen_logo);
        commentsListView = (ListView) findViewById(R.id.main_screen_comments_list_view);
        scrollView = (ScrollView) findViewById(R.id.main_screen_scroll_view);

        //Map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapFragment);
        mapFragment.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        mapFragment.getMapAsync(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler = new MainScreenHandler();
        if(task == null)
            requestTaskInfo();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()== android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("taskId", taskId);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(taskId == null) {
            taskId = savedInstanceState.getString("taskId");
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    private void requestTaskInfo(){
        loadingBar.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.INVISIBLE);
        AsyncGetTask requestJob = new AsyncGetTask(handler);
        String apiPath = Constants.TASKS_API_PATH + taskId;
        requestJob.execute(apiPath);
    }

    private void renderTaskInfo(){
        String html  = "<html><hr/><body style=\"text-align:justify;color:#CB8100;\">%s</body></html>";
        textTextView.loadData(String.format(html, task.getText()), "text/html", "utf-8");
        setTitle(task.getCity() + " - " + task.getNeighborhood());
        pictureImageView.setImageDrawable(task.getPicture());
        logo.setImageDrawable(task.getLogo());
        titleTextView.setText(task.getTitle());
        mapAddressTextView.setText(task.getAddress());
        setMapMarker();
        commentsListView.setAdapter(new CommentAdapter(task.getCommentsList(), getApplicationContext(), handler));
        loadingBar.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
    }

    private void setMapMarker(){
        if(map != null) {
            LatLng position = new LatLng(task.getLatitude(), task.getLongitude());
            map.addMarker(new MarkerOptions().position(position));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 16));
        }
    }

    private void setErrorMessage(){
        Toast errorToast = Toast.makeText(this, R.string.request_network_issue, Toast.LENGTH_LONG);
        errorToast.show();
    }

    public void onClickCallPhone(View view){
        Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
        phoneIntent.setData(Uri.parse("tel:"+task.getPhoneNumber()));
        phoneIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(phoneIntent);
    }

    public void onClickServices(View view){
        Intent serviceIntent = new Intent(this,ServicesActivity.class);
        startActivity(serviceIntent);
    }

    public void onClickAddress(View view){
        Toast addressToast = Toast.makeText(getApplicationContext(), task.getAddress(), Toast.LENGTH_LONG);
        addressToast.show();
    }

    public void onClickComments(View view){
        if(task.getCommentsList().isEmpty()){
            Toast noCommentToast = Toast.makeText(getApplicationContext(), getString(R.string.no_comment), Toast.LENGTH_SHORT);
            noCommentToast.show();
        }else{
            scrollView.fullScroll(View.FOCUS_DOWN);
        }
    }

    public void onClickFavorites(View view){
        return;
    }


    private class MainScreenHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == Constants.SUCCESSFULL_RESPONSE) {
                task = (Task) msg.obj;
                renderTaskInfo();
            }else if (msg.what == Constants.PICTURE_RECEIVED){
                ArrayAdapter adapter = (ArrayAdapter) commentsListView.getAdapter();
                adapter.notifyDataSetChanged();
            }else{
                setErrorMessage();
            }
            scrollView.fullScroll(View.FOCUS_UP);
        }
    }
}
