package br.com.gustavokath.a4allapp.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by gustavokath on 05/02/17.
 */

public class TaskIdList {
    @JsonProperty("lista")
    private List<String> idList;

    public List<String> getIdList() {
        return idList;
    }

    public void setIdList(List<String> idList) {
        this.idList = idList;
    }
}
