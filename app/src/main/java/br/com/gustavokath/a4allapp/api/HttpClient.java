package br.com.gustavokath.a4allapp.api;

import java.io.IOException;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by gustavokath on 02/02/17.
 */
public class HttpClient {
    private static HttpClient httpClient = new HttpClient();

    public static HttpClient getInstance() {
        return httpClient;
    }

    private OkHttpClient client;

    private HttpClient() {
        client = new OkHttpClient();
    }

    public Response get(URL url) throws IOException{
        Request request = new Request.Builder().url(url).build();

        Response response = client.newCall(request).execute();
        return response;
    }

}
