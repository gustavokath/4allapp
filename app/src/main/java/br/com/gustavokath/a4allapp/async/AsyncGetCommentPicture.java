package br.com.gustavokath.a4allapp.async;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import br.com.gustavokath.a4allapp.api.HttpClient;
import br.com.gustavokath.a4allapp.constants.Constants;
import br.com.gustavokath.a4allapp.entities.Comment;
import br.com.gustavokath.a4allapp.entities.Task;
import okhttp3.Response;

/**
 * Created by gustavokath on 02/02/17.
 */

public class AsyncGetCommentPicture extends AsyncHttpRequest<Comment, Void, Boolean> {


    public AsyncGetCommentPicture(Handler handler) {
        super(handler);
    }

    @Override
    protected Boolean doInBackground(Comment... params) {
        Comment comment = params[0];
        if(comment == null)
            return false;

        Drawable picture = getPicture(comment.getUrlPicture(), "comment");
        if(picture != null){
            comment.setPicture(picture);
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean task) {
        int messageCode;
        if(task == null){
            messageCode = Constants.ASYNC_JOB_FAILED;
        }else{
            messageCode = Constants.PICTURE_RECEIVED;
        }

        Message message = new Message();
        message.what = messageCode;
        getHandler().sendMessage(message);
    }
}
