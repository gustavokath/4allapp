package br.com.gustavokath.a4allapp.constants;

/**
 * Created by gustavokath on 02/02/17.
 */

public class Constants {
    public static final String HTTP_PROTOCOL = "http";

    public static final String API_4ALL_URI = "dev.4all.com";

    public static final String TASKS_API_PATH = "tarefa/";

    public static final int API_4ALL_PORT = 3003;

    public static final int ASYNC_JOB_FAILED = 1;

    public static final int SUCCESSFULL_RESPONSE = 2;

    public static final int ERROR_RESPONSE = 3;

    public static final int PICTURE_RECEIVED = 21;
}
