package br.com.gustavokath.a4allapp.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

import br.com.gustavokath.a4allapp.R;
import br.com.gustavokath.a4allapp.async.AsyncGetCommentPicture;
import br.com.gustavokath.a4allapp.entities.Comment;

public class CommentAdapter extends ArrayAdapter<Comment> {
        private List<Comment> commentList;
        private Context context;
        private Handler handler;

        public CommentAdapter(List<Comment> commentList, Context context, Handler handler){
            super(context,R.layout.comment_list_item , commentList);
            this.commentList = commentList;
            this.context = context;
            this.handler = handler;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.comment_list_item, parent, false);
            }

            CircularImageView imageView = (CircularImageView) convertView.findViewById(R.id.comment_item_picture);
            TextView nameTextView = (TextView) convertView.findViewById(R.id.comment_item_name);
            TextView titleTextView = (TextView) convertView.findViewById(R.id.comment_item_title);
            TextView textTextView = (TextView) convertView.findViewById(R.id.comment_item_text);
            RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.comment_item_rating);

            Comment comment = commentList.get(position);

            nameTextView.setText(comment.getName());
            titleTextView.setText(comment.getTitle());
            textTextView.setText(comment.getText());
            ratingBar.setRating(comment.getScore());

            if(comment.getPicture() == null){
                AsyncGetCommentPicture asyncGetCommentPicture = new AsyncGetCommentPicture(handler);
                asyncGetCommentPicture.execute(comment);
            }else{
                imageView.setImageDrawable(comment.getPicture());
            }

            return convertView;
        }
    }