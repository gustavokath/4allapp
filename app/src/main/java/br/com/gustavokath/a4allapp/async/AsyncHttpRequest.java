package br.com.gustavokath.a4allapp.async;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import br.com.gustavokath.a4allapp.api.HttpClient;
import okhttp3.Response;

/**
 * Created by gustavokath on 05/02/17.
 */

public abstract class AsyncHttpRequest<T,U,V> extends AsyncTask<T, U, V>{

    private Handler handler;

    public AsyncHttpRequest(Handler handler) {
        this.handler = handler;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    protected Drawable getPicture(String fullUrl, String imageName){
        URL url = buildUrl(fullUrl);
        if(url == null)
            return null;

        Drawable drawable = null;
        try{
            InputStream stream = (InputStream) url.getContent();
            drawable = Drawable.createFromStream(stream, imageName);
        }catch (IOException exception){
            Log.e("ERROR","Fail to get picture");
        }
        return drawable;
    }

    protected URL buildUrl(String method, String server, int port, String path){
        URL url = null;
        try {
            url = new URL(method, server, port, path);
        }catch (MalformedURLException exception){
            exception.printStackTrace();
            Log.e("ERROR","Error creating request URL");
        }
        return url;
    }

    protected URL buildUrl(String strUrl){
        URL url = null;
        try {
            url = new URL(strUrl);
        }catch (MalformedURLException exception){
            exception.printStackTrace();
            Log.e("ERROR","Error creating request URL");
        }
        return url;
    }

    protected Response getRequest(URL url){
        HttpClient client = HttpClient.getInstance();
        Response response = null;

        try {
            response = client.get(url);
        } catch (IOException exception) {
            exception.printStackTrace();
            Log.e("ERROR","Error making the API request");
        }

        return response;
    }


}
