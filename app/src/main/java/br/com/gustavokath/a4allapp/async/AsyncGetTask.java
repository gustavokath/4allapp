package br.com.gustavokath.a4allapp.async;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import br.com.gustavokath.a4allapp.api.HttpClient;
import br.com.gustavokath.a4allapp.constants.Constants;
import br.com.gustavokath.a4allapp.entities.Task;
import okhttp3.Response;

/**
 * Created by gustavokath on 02/02/17.
 */

public class AsyncGetTask extends AsyncHttpRequest<String, Void, Task> {

    public AsyncGetTask(Handler handler) {
        super(handler);
    }

    @Override
    protected Task doInBackground(String... params) {
        Task task = getTask(params[0]);
        if(task == null)
            return null;

        Drawable picture = getPicture(task.getUrlPicture(), "picture");
        if(picture != null){
            task.setPicture(picture);
        }

        Drawable logo = getPicture(task.getUrlLogo(), "logo");
        if(picture != null){
            task.setLogo(logo);
        }

        return task;
    }

    @Override
    protected void onPostExecute(Task task) {
        int messageCode;
        if(task == null){
            messageCode = Constants.ASYNC_JOB_FAILED;
        }else{
            messageCode = Constants.SUCCESSFULL_RESPONSE;
        }

        Message message = new Message();
        message.what = messageCode;
        message.obj = task;
        getHandler().sendMessage(message);
    }

    private Task getTask(String path){
        Task task = null;
        URL url = buildUrl(Constants.HTTP_PROTOCOL, Constants.API_4ALL_URI, Constants.API_4ALL_PORT, path);
        if(url == null)
            return null;

        Response response = getRequest(url);
        if(response == null)
            return null;

        if(response.isSuccessful()){
            try {
                task = new ObjectMapper().readValue(response.body().string(), Task.class);
            }catch (IOException exception){
                Log.e("ERROR","Fail to deserialize task");
            }
        }
        return task;
    }
}
