package br.com.gustavokath.a4allapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import br.com.gustavokath.a4allapp.R;
import br.com.gustavokath.a4allapp.async.AsyncGetTaskIdList;
import br.com.gustavokath.a4allapp.constants.Constants;
import br.com.gustavokath.a4allapp.entities.TaskIdList;

public class FirstScreenActivity extends AppCompatActivity {
    private ListView tasksListView;
    private ArrayAdapter listViewAdapter;
    private ProgressBar loadingBar;
    private Handler handler;
    private TaskIdList taskIdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tasksListView = (ListView) findViewById(R.id.tasks_list_view);
        loadingBar = (ProgressBar) findViewById(R.id.loading_progress_bar);
        handler = new FirstScreenHandler();
        listViewAdapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1);
        tasksListView.setAdapter(listViewAdapter);

        tasksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String taskId = String.valueOf(adapterView.getItemAtPosition(position));
                Intent intent = new Intent(getApplicationContext(), MainScreenActivity.class);
                intent.putExtra("id", taskId);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestTasksIds();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tela_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void requestTasksIds(){
        loadingBar.setVisibility(View.VISIBLE);
        AsyncGetTaskIdList requestJob = new AsyncGetTaskIdList(handler);
        requestJob.execute(Constants.TASKS_API_PATH);
    }

    private void renderTasks(TaskIdList tasks){
        if(tasks != null) {
            taskIdList = tasks;
            listViewAdapter.clear();
            listViewAdapter.addAll(tasks.getIdList());
            listViewAdapter.notifyDataSetChanged();
        }
    }

    private ArrayList<String> convertJsonToArrayList(JSONArray jsonArray) throws JSONException{
        if(jsonArray == null)
            return null;

        ArrayList<String> list = new ArrayList<>();
        for(int i=0;i<jsonArray.length();i++){
            list.add(jsonArray.get(i).toString());
        }
        return list;
    }

    private void setErrorMessage(){
        Toast errorToast = Toast.makeText(this, R.string.request_network_issue, Toast.LENGTH_LONG);

        listViewAdapter.clear();
        listViewAdapter.add(getString(R.string.request_network_issue));
        listViewAdapter.notifyDataSetChanged();

        errorToast.show();
    }

    private class FirstScreenHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            loadingBar.setVisibility(View.GONE);
            if(msg.what == Constants.SUCCESSFULL_RESPONSE){
                TaskIdList idList = (TaskIdList) msg.obj;
                renderTasks(idList);
            }else{
                setErrorMessage();
            }
        }
    }

}
